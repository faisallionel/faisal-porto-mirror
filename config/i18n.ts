const DEFAULT_LOCALE = 'id'
const DEFAULT_LOCALE_ISO_CODE = 'id'

const LOCALES = [{ code: 'id', file: 'id.json', iso: 'id' }]

export { DEFAULT_LOCALE, DEFAULT_LOCALE_ISO_CODE, LOCALES }