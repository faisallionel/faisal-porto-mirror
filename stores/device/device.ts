export enum DeviceType {
    Mobile = "mobile",
    Tablet = "tablet",
    TabletLandscape = "tabletLandscape",
    Laptop = "laptop",
    Desktop = "desktop",
}

export const useDeviceType = defineStore('deviceType', {
    state: () => ({
        deviceType: DeviceType.Mobile,
    }),
    actions: {
        async getDevice() {
            if (typeof window !== 'undefined') {
                const mobileQuery = window.matchMedia('(min-width: 0px) and (max-width: 425px)').matches;
                const tabletQuery = window.matchMedia('(min-width: 426px) and (max-width: 768px)').matches;
                const tabletLandscapeQuery = window.matchMedia('(min-width: 769px) and (max-width: 872px)').matches;
                const laptopQuery = window.matchMedia('(min-width: 873px) and (max-width: 1199px)').matches;
                const desktopQuery = window.matchMedia('(min-width: 1200px)').matches;
    
                if (mobileQuery) {
                    this.deviceType = DeviceType.Mobile
                }
                if (tabletQuery) {
                    this.deviceType = DeviceType.Tablet
                }
                if (tabletLandscapeQuery) {
                    this.deviceType = DeviceType.TabletLandscape
                }
                if (laptopQuery) {
                    this.deviceType = DeviceType.Laptop
                }
                if (desktopQuery) {
                    this.deviceType = DeviceType.Desktop
                }
                // console.table(["mobile : " + mobileQuery, "tablet landscape : " + tabletLandscapeQuery, "tablet : " + tabletQuery, "laptop : " + laptopQuery, "desktop : " + desktopQuery])
            }
        },
        initialize() {
            this.getDevice()
            onMounted(() => {
                window.addEventListener('resize', () => this.getDevice());
            })
            onBeforeUnmount(() => {
                window.removeEventListener('resize', () => this.getDevice());
            })
        }
    },
})