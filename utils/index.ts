export const formatNumber = (number: number) => Intl.NumberFormat('en-GB', {
    notation: 'compact',
    maximumFractionDigits: 1
}).format(number)
