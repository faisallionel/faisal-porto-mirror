const listTimeline = [
    {
        timeline: `May 2022 - <b class="font-weight-bold"> Present </b>`, title: "Software Engineer at BTPN Sharia, Jakarta"
    },
    {
        timeline: "February 2022 - October 2022", title: "Mobile Android Developer at Harmoni Panca Utama, Jakarta",
    },
    {
        timeline: "January 2022 - Mar 2022", title: "Part Time Software Engineer Trainee at HubbedIn, Singapore"
    },
    {
        timeline: "July 2020 - May 2022", title: "Fullstack Engineer at Kano Teknologi Utama, Surabaya"
    },
    {
        timeline: "July 2018 - Sep 2018", title: "Internship Trainee at EACIIT (Big Data & Digitisation), Surabaya"
    },
]

export default listTimeline;