const listSkillDemo = [
    {id: 1, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Personal Branding Faisal)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYVxTh9L2CvfLLCWthRyERCy"},
    {id: 2, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Portfolio Showcase)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYVlH07cDXu7zSZrK0E3RH9V"},
    {id: 3, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Btpn Sharia Portfolio)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYXcMD1v1vxHcGx5dRaG2QtW"},
    {id: 4, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Backend Roadmap)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYXiWCJh0GQpdTju7xlJc-bz"},
    {id: 5, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Devops Roadmap)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYWKcVtZZfE-NhdaQvO-SQGa"},
    {id: 6, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Frontend Roadmap)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYUQUAqNzzpFaPp71laZGobl"},
    {id: 7, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Mobile Roadmap)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYUOJD_dZ960gpJAbv4f-Cxd"},
    {id: 8, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Blockchain Roadmap)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYWkPkhO2PNcCYInZZIxV_gL"},
    {id: 9, timeline: "14 Maret 2024", skillset: "Youtube Playlist (QA Roadmap)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYWMzcQqx9rb2ump1txtwojF"},
    {id: 10, timeline: "14 Maret 2024", skillset: "Youtube Playlist (Study Case)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYXQ5Niqpq9eAcML-Tnt7K2J"},
    {id: 11, timeline: "14 Maret 2024", skillset: "Youtube Playlist (System Design)", url: "https://www.youtube.com/playlist?list=PLl-4LPxiKoYVqu9JFYsZRjcBrnL10RKC7"},
]

export default listSkillDemo;