import type { ListPortfolioResponse } from "~/server/api/portfolio/models"


export const usePortfolioStore = defineStore('portfolio', {
    state: () => {
        return {
            router: useRouter(),
            routes: useRoute(),
            dataPortfolio: [] as ListPortfolioResponse[],
            filterPortfolio: [] as ListPortfolioResponse[],
            search: "",
            isShowModal: false,
            isActivePortofolio: 0 as number,
            isLoadingPortfolio: false,
            category: {
                mvp: "Konsep ini menekankan pada pengembangan produk dengan fitur minimum yang dapat memberikan nilai yang signifikan kepada pengguna. Tujuannya adalah untuk memvalidasi asumsi dan mengumpulkan umpan balik pelanggan secepat mungkin.",
                mmp: "MMP adalah konsep di mana produk memiliki fitur minimum yang diperlukan untuk menjadi layak di pasar dan menarik bagi pelanggan. Ini memungkinkan produk untuk segera memasuki pasar dan memulai penghasilan.",
                opensource: "Proyek perangkat lunak yang kode sumbernya tersedia untuk umum",
                studycase: "Proyek perangkat lunak berdasarkan studi kasus",
                labs: "Proyek perangkat lunak hasil eksperimen",
            },
        }
    },
    actions: {
        async getDataPortfolio() {
            const data = await $fetch('/api/portfolio')
            this.dataPortfolio = data
        },
        setModal(index: number) {
            this.isShowModal = true
            this.isActivePortofolio = index
        },
        backModal() {
            if (this.isActivePortofolio != 0) {
                this.isActivePortofolio -= 1
            }
        },
        nextModal() {
            if (this.dataPortfolio != null && this.isActivePortofolio != this.dataPortfolio.length - 1) {
                this.isActivePortofolio += 1
            }
        },
        closeModal() {
            this.isShowModal = false
        },
        goToFilter(filter: string) {
            this.router.push("?filter=" + filter)
        },
        transformDataPortfolio(): ListPortfolioResponse[] {
            if (this.search != "" && this.routes.query?.filter == undefined) {
                return this.dataPortfolio?.filter(item => item.title.toLowerCase().includes(this.search.toLowerCase()))
            }
            if (this.search != "" && this.routes.query?.filter != undefined) {
                return this.dataPortfolio?.filter(item => item.tags.some(data => data == this.routes.query?.filter) && item.title.toLowerCase().includes(this.search.toLowerCase()))
            }
            if (this.routes.query?.filter != undefined) {
                return this.dataPortfolio?.filter(item => item.tags.some(data => data == this.routes.query?.filter))
            }
            return this.dataPortfolio
        },
        inputSearch(data: any) {
            this.isLoadingPortfolio = true
            this.search = data.target.value
            setTimeout(() => {
                this.isLoadingPortfolio = false
                this.transformDataPortfolio()
            }, 1000);
        }
    },
})
