export default defineNuxtConfig({
  ssr: false,

  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
    }
  },

  imports: {
    dirs: [
      "composables/**"
    ]
  },

  plugins: [
    '@/plugins/antd',
    '@/plugins/api',
  ],

  sourcemap: true,
  debug: true,

  runtimeConfig: {
    public: {
      apiBaseUrl: process.env.NUXT_API_BASE_URL,
      apiKey: process.env.NUXT_API_KEY
    }
  },

  css: [
    '@/assets/css/main.scss',
    '@/assets/css/reset.scss',
    '@/assets/css/animation.scss',
    '@/assets/css/font.scss',
    '@/assets/css/spacing.scss',
  ],

  vite: {
    ssr: {
      noExternal: ['moment', 'compute-scroll-into-view', 'ant-design-vue', '@ant-design/icons-vue'],
    },
  },

  devtools: {
    enabled: true,
    timeline: {
      enabled: true
    }
  },

  modules: [
    "@pinia/nuxt",
    "@morev/vue-transitions/nuxt",
    "@nuxtjs/i18n"
  ],
  pinia: {
    storesDirs: ['./stores/**'],
  },

  i18n: {
    compilation: {
      strictMessage: false,
      escapeHtml: false
    },
    locales: [
      {
        code: 'en',
        file: 'en-US.js'
      },
      {
        code: 'id',
        file: 'id-ID.js',
      }
    ],
    lazy: false,
    langDir: 'lang',
    defaultLocale: 'id'
  },
})