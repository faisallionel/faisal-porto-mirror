export type ResponseSuccess = {
    data: any
    success: boolean
    code: number

}
export type ResponseError = {
    data: any
    success: boolean
    code: number
}