import { AxiosResponse } from "axios";
import chalk from "chalk";

export function GetLogger(event: AxiosResponse<any, any>) {
    console.groupCollapsed(chalk.bgGreen.white(`${event.config.method?.toUpperCase()} ${event.config.url} - ${event.status} (${event.headers['request-duration']} ms)`));
    console.table(
        {
            status: event.status,
            method: event.config.method?.toUpperCase(),
            url: event.config.url,
            auth: event.config.auth,
            baseUrl: event.config.baseURL,
            payload: event.config.params,
        },
    );
    console.groupEnd();
}