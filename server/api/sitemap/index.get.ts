export default defineSitemapEventHandler(async (e) => {
    const posts = await Promise.all([
        { 
            _path: "/",
            modifiedAt: new Date()
        },
        { 
            _path: "/about",
            modifiedAt: new Date()
        },
        { 
            _path: "/album",
            modifiedAt: new Date()
        },
        { 
            _path: "/contact",
            modifiedAt: new Date()
        },
        { 
            _path: "/news",
            modifiedAt: new Date()
        },
        { 
            _path: "/portofolio",
            modifiedAt: new Date()
        },
        { 
            _path: "/timeline/detail",
            modifiedAt: new Date()
        },
        { 
            _path: "/timeline",
            modifiedAt: new Date()
        },
    ]);
    return posts.map((p) => {
        return {
            loc: p._path,
            lastmod: p.modifiedAt,
            priority: 1.0
        };
    });
});
