
import HttpClient from "../../utils/axios"
import { ResponseError, ResponseSuccess } from "../../utils/response"
import { GetLogger } from "../../utils/logger"
import { xml2json } from "xml-js"
import { ListNewsResponse } from "./models"
import { parse } from 'node-html-parser';

export default defineEventHandler(async (event) => {
    const res = await HttpClient({
        method: "GET",
        url: "https://medium.com/feed/@faisal-affan",
    })
    GetLogger(res)

    if (res.status == 200) {
        const transformToJson = JSON.parse(xml2json(res.data, { compact: true, spaces: 4 }))
        console.log(transformToJson.rss.channel.item[0])

        let transform: ListNewsResponse[] = [];

        transformToJson.rss.channel.item.map((val: any) => {
            const doc = parse(val['content:encoded']._cdata);
            const imgElement = doc.querySelector('img');

            let thumbnailSrc = ""

            if (imgElement) {
                const imgSrc = imgElement.getAttribute('src');
                thumbnailSrc = imgSrc as string
            }
            
            transform.push(<ListNewsResponse>{
                title: val.title,
                category: val.category,
                content_encoded: val['content:encoded'],
                atom_updated: val['atom:updated'],
                dc_creator: val['dc:creator'],
                guid: val.guid,
                link: val.link,
                pub_date: val.pubDate,
                thumbnail: thumbnailSrc
            })
        })
        setResponseStatus(event, 200)
        return {
            success: true,
            data: transform as ListNewsResponse[],
        }
    } else {
        throw createError(<ResponseError>{
            code: 400,
            success: false
        })
    }
})