export type ListNewsResponse = {
    title?: {
        _cdata?: string;
    };
    link?: {
        _text?: string;
    };
    guid?: {
        _attributes?: {
            isPermaLink?: string;
        };
        _text?: string;
    };
    category: {
        _cdata?: string;
    }[];
    dc_creator?: {
        _cdata?: string;
    };
    pub_date?: {
        _text?: string;
    };
    atom_updated?: {
        _text?: string;
    };
    content_encoded: {
        _cdata?: string;
    };
    thumbnail: string
}
