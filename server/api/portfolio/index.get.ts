import { ListPortfolioResponse } from "./models"
import DataPortfolioLocal from "./data";

export default defineEventHandler(async (event) => {
    return DataPortfolioLocal as ListPortfolioResponse[];
})