import { ListPortfolioResponse } from "./models"

const data = [
    {
        "key": "sitepat",
        "title": "SITepat",
        "desc": "E-Financing",
        "img_thumbnail": "/portfolio/sitepat/banner.png",
        "yt_link": "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
        "video_youtube": "",
        "tags": ["finance", "banking", "mvp", "mmp"],
    },
    {
        "key": "acquisition",
        "title": "Terra IF Acqusition",
        "desc": "E-Financing",
        "img_thumbnail": "/portfolio/acquisition/banner.png",
        "video_youtube": "",
        "tags": ["finance", "banking", "mvp", "mmp"],
    },
    {
        "key": "minerva",
        "title": "New Minerva",
        "desc": "Mining Coal Web Apps",
        "img_thumbnail": "/portfolio/minerva/banner.png",
        "video_youtube": "",
        "tags": ["finance", "banking", "mvp"],
    },
    {
        "key": "cybera",
        "title": "IAG Cybera 360",
        "desc": "Cyber Attack Insurance",
        "img_thumbnail": "/portfolio/cybera/banner.png",
        "video_youtube": "",
        "tags": ["finance", "banking", "mvp"],
    },
    {
        "key": "youtama",
        "title": "Youtama",
        "desc": "Clothing and inventory",
        "img_thumbnail": "/portfolio/youtama/banner.png",
        "video_youtube": "",
        "tags": ["clothing", "inventories", "studycase"],
    },
] as ListPortfolioResponse[]

export default data