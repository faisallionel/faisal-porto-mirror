export type ListPortfolioResponse = {
    key: string
    title: string
    desc: string
    img_thumbnail: string
    video_youtube: string
    content: string
    tags: string[]
}
