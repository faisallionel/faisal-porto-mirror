export default {
    "about": {
        "title_about": "about",
        "desc_1": `Hello, I'm <b class='font-weight-bold'>Faisal Affan</b> Senior Software Engineer who has been working in the programming field for 6 years at :`,
        "desc_speciality": `Yang berspesialis di bidang <b class='font-weight-bold'>Backend Engineering</b>`,
        "why_me": `Why Me ?`,
        "why_me_list": {
            "one": `<li style="list-style: none"> - saya memiliki general knowledge seperti arsitektur aplikasi, big data, data science dan devops. dengan berspesialis sebagai backend engineer. </li>`,
            "two": `<li style="list-style: none"> - saya memilki pengalaman di berbagai project MVP seperti sitepat, acqusition, minerva, cybera360 dan hpu. <a href="/portofolio">Lihat Portofolio Saya</a> </li>`,
            "three": `<li style="list-style: none"> - saya berpengalaman bekerja di berbagai budaya seperti hybrid, remote dan on site </li>`,
            "four": `<li style="list-style: none"> - dan juga berbagai tipe perusahaan dari startup, corporate dan software house. dari develop produk keuangan hingga edukasi </li>`,
            "five": `<li style="list-style: none"> - dari sekian banyak history pekerjaan. saya memiliki feedback positif dari rekan” pekerja saya. karena saya telah membantu mereka </li>`,
            "six": `<li style="list-style: none"> - saya memiliki sertifikasi di masing” bidang </li>`,
            "seven": `<li style="list-style: none"> - saya juga active dalam berbagai platform: <a href="">Github</a>, <a href="">Stackoverflow</a>, <a href="">linkedin</a>, <a href="">Dev Community</a>, <a href="">Medium</a> </li>`,
        },
        "why_me_2": `Back to topic, Why Me ?`,
        "why_me_list_2": {
            "one": `<li style="list-style: none"> - jika harga developer dan kemahiran diukur berdasarkan skillset maka tau harga yang dibayar dan didapat perusahaan berapa ? </li>`,
            "two": `<li style="list-style: none"> - Jika harga developer berdasarkan pengalaman kerja </li>`,
            "three": `<li style="list-style: none"> - Jika harga developer berdasarkan waktu pengerjaan bandingkan skillset yang sudah saya punya dengan berpikir” berpaling ke developer lain ? apakah anda rela me training developer dulu untuk mengerjakan project yang load nya sudah cepat ? </li>`,
            "four": `<li style="list-style: none"> - Jika harga developer berdasarkan <a target="_blank" href="https://www.linkedin.com/in/faisal-affan/details/certifications/"> sertifikasi</a> ? </li>`,
            "five": `<li style="list-style: none"> - Jika harga developer berdasarkan <a target="_blank" href="https://www.linkedin.com/in/faisal-affan/details/recommendations/"> tesimonial</a> ? </li>`,
        },
    }
};